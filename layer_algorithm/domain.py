import math
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle

class Domain(object):
    '''Partitioned two dimensional computational domain.

    Defines a two dimensional domain of size :math:`L_x\\times L_y`, which is 
    partitioned into :math:`M_x\\times M_y` cells of size :math`w_x\\times w_y`:
    with :math:`w_i = L_i/M_i` for directions :math:`i=x,y`. Boundary regions of width
    :math:`w_x` and :math:`w_y` are added to treat periodic boundary conditions.
    More specifically, in direction :math:`i` the interior corresponds to the range
    :math:`[w_i,L_i+w_i[`, and the boundaries correspond to :math:`[0,w_i[` and 
    :math:`[L_i+w_i,L_i+2w_i[`.
    
    Cells are numbered consecutively, with the y-index running fastest, i.e.
    :math:`i=(M_y+2)i_x+i_y`.

    :arg Lx: Size of domain in x-direction
    :arg Ly: Size of domain in y-direction
    :arg Mx: Number of cells in x-direction
    :arg My: Number of cells in y-direction
    '''
    def __init__(self,Lx,Ly,Mx,My):
        self.Lx = Lx
        self.Ly = Ly
        self.Mx = Mx
        self.My = My
        offset_1d = [-1,0,1]
        self._offset = np.zeros(9,dtype=int)
        i = 0
        for iy in range(3):
            for ix in range(3):
                self._offset[i] = offset_1d[ix]*(self.My+2)+offset_1d[iy]
                i += 1

    @property
    def offset(self):
        '''Array of offsets to all 9 neighbouring cells (including cell itself).
        
        The entries of this array of length 9 are integers, giving the relativ index in
        the consecutive numbering of cells.
        '''
        return self._offset

    @property
    def wx(self):
        '''Cell size in x-direction.'''
        return self.Lx/float(self.Mx)

    @property
    def wy(self):
        '''Cell size in y-direction.'''
        return self.Ly/float(self.My)

    @property
    def xmin(self):
        '''Lower limit of x-coordinate (including boundary).'''
        return 0

    @property
    def xmax(self):
        '''Upper limit of x-coordinate (including boundary).'''
        return 2*self.wx+self.Lx

    @property
    def ymin(self):
        '''Lower limit of y-coordinate (including boundary).'''
        return 0

    @property
    def ymax(self):
        '''Upper limit of y-coordinate (including boundary).'''
        return 2*self.wy+self.Ly

    @property
    def xmin_int(self):
        '''Lower limit of x-coordinate in interior.'''
        return self.wx

    @property
    def xmax_int(self):
        '''Upper limit of x-coordinate in interior.'''
        return self.wx+self.Lx

    @property
    def ymin_int(self):
        '''Lower limit of y-coordinate in interior.'''
        return self.wy

    @property
    def ymax_int(self):
        '''Upper limit of y-coordinate in interior.'''
        return self.wy+self.Ly

    @property
    def ninteriorcells(self):
        '''Number of interior cells.'''
        return self.Mx*self.My

    @property
    def ncells(self):
        '''Number of all cells.'''
        return (self.Mx+2)*(self.My+2)

    def isinterior(self,idx):
        '''Return true of cell with index i is interior cell.

        :arg idx: Cell index :math:`i=`
        '''
        ix = idx/(self.My+2)
        iy = idx - ix*(self.My+2)
        return ( (ix > 0) and (ix < self.Mx+1) and (iy > 0) and (iy < self.My+1) )

    def cellidx(self,rx,ry):
        '''Cell index of particle with position :math:`\\vec{r}=(r_x,r_y)`

        This returns the scalar index from the consecutive cell numbering.
    
        :arg rx: x-coordinate of particle
        :arg ry: y-coordinate of particle
        '''
        return (self.My+2)*int(math.floor(rx/self.wx))+int(math.floor(ry/self.wy))

    def draw(self):
        '''Draw the domain.

        Draw the entire domain, highlight the interior and show the individual cells.
        '''
        plt.xlim(self.xmin,self.xmax)
        plt.ylim(self.ymin,self.ymax)
        ax = plt.gca()
        # Draw rectangle around entire domain (including boundary)
        ax.add_patch(Rectangle((self.xmin,self.xmax),
                                self.ymin,self.ymax,
                                facecolor='gray',linewidth=2))
        # Highlight interior of domain
        ax.add_patch(Rectangle((self.xmin_int,self.ymin_int),
                                self.Lx,self.Ly,
                                facecolor='white',linewidth=2))
        # Draw lines showing the individual cells
        for i in range(self.Mx+1):
            y = self.ymin_int+i*self.wy
            plt.plot((self.xmin,self.xmax),(y,y),linewidth=1,linestyle='--',color='black')
        for j in range(self.My+1):
            x = self.xmin_int+j*self.wx
            plt.plot((x,x),(self.ymin,self.ymax),linewidth=1,linestyle='--',color='black')
        ax.set_xticks((self.xmin_int,self.xmax_int))
        ax.set_yticks((self.ymin_int,self.ymax_int))
        ax.set_xticklabels((('%3.1f' % 0.0),('%3.1f' % self.Lx)))
        ax.set_yticklabels((('%3.1f' % 0.0),('%3.1f' % self.Ly)))

