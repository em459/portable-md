import os
import sys
from domain import *
from state import *
from potential import *
from integrator import *

npart = 64
nsteps=128
dt = 0.001

interior_only=False

def outputfilename(outputdir,i):
    return os.path.join(outputdir,'state_'+('%03d' % i)+'.png')

outputdir=os.path.join(os.path.expanduser('~'),'tmp/output_md')
domain = Domain(1.0, 1.0, 4,4)
potential = LennartJones(1.0,0.1)
state = State(domain,potential,npart,1.0)
integrator = Leapfrog(state,dt)
state.savefig(outputfilename(outputdir,0),interior_only)
for i in range(nsteps):
    print i, state.total_energy()
    integrator.step()
    state.savefig(outputfilename(outputdir,i+1),interior_only)
