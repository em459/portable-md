import numpy as np
from matplotlib import pyplot as plt

from domain import *
from potential import *

class State(object):
    '''State of atomic system.

    Stores the positions, velocities and accelerations of a set of particles
    confined to a two dimensional domain, which interact via a given short range
    potential. The class provides a method for calculating the interatomic forces.
    
    The layer method described in the paper 'Large Scale Molecular Dynamics Simulation
    using Vector and Parallel Computers', D.C. Rapoport, North Holland Publishing (1988)
    available at www.ph.biu.ac.il/~rapaport/papers/88b-cpr.pdf is used for the force
    calculation. The number of layers used is set to 8 times the average number of particles
    per cell.

    For all variables arrays which can hold up to npart + 4*npart_bound particles are
    allocated, where npart_bound is the expected number of particles in the boundary cells.
    The first npart entries in the arrays are the particles in the interior of the domain.

    :arg domain: Computational domain, of type :class:`.Domain`
    :arg potential: Interatomic potential, has to be a type derived from the
        abstract base class :class:`.Potential`
    :arg npart: Number of particles
    :arg mass: Mass of one particle
    '''
    def __init__(self,domain,potential,npart,mass):
        self._domain = domain
        self._potential = potential
        # Number of interior particles
        self._npart = npart
        # Number of total particles (including the ones duplicated on the boundary)
        self._npart_total = self._npart
        self._mass = mass
        self._inv_mass = 1./self._mass
        self._maxlayer=8*self._npart/self._domain.ninteriorcells
        # Space to allocate for particles
        self._npart_allocated = int(self._npart*(1+4*(1.-float(self.domain.ninteriorcells)/float(self.domain.ncells))))
        # Particle positions
        self.rx = np.zeros(self._npart_allocated)
        self.ry = np.zeros(self._npart_allocated)
        # Particle velocities
        self.vx = np.zeros(self._npart_allocated)
        self.vy = np.zeros(self._npart_allocated)
        # Particle accelations
        self.ax = np.zeros(self._npart_allocated)
        self.ay = np.zeros(self._npart_allocated)
        # Potential energy
        self._pe = np.zeros(self._npart_allocated)
        # Data structures for building compressed layer list
        self._layerlist = np.zeros(self._npart_allocated,dtype=int)
        self._bmask = np.zeros(self._npart_allocated,dtype=int)
        self._qoffset = np.zeros(self._maxlayer+1,dtype=int)
        self._cell_idx = np.zeros(self._npart_allocated,dtype=int)
        self.arrange_positions()
        self.randomize_velocities()

    @property
    def domain(self):
        '''Return computational domain.'''
        return self._domain

    @property
    def potential(self):
        '''Return interatomic potential.'''
        return self._potential

    @property
    def npart(self):
        '''Return number of particles.'''
        return self._npart

    @property
    def mass(self):
        '''Mass of one particle.'''
        return self._mass

    def kinetic_energy(self):
        '''Calculate the kinetic energy of all interior particles.'''
        ke = 0.0
        for vx,vy in zip(self.vx[0:self._npart],self.vy[0:self._npart]):
            ke += 0.5*self._mass*(vx**2+vy**2)
        return ke

    def potential_energy(self):
        '''Return the potential energy of all interior particles.'''
        return np.sum(self._pe[0:self._npart])

    def total_energy(self):
        '''Return the total (kinetic + potential) energy.

        This should be conserved in the continuum limit, but will ony be conserved
        approximately for most numerical integrators.
        '''
        return self.potential_energy() + self.kinetic_energy()

    def randomize_velocities(self):
        '''Set the velocities of the interior particles to random values.'''
        np.random.seed(239351397)
        self.vx[0:self.npart] = np.random.randn(self.npart)
        self.vy[0:self.npart] = np.random.randn(self.npart)
    
    def arrange_positions(self):
        '''Arrange the (interior) particle positions in a regular grid in the domain.'''
        k = 0
        # Square root of the number of particles (rounded up)
        sn = float(math.ceil(math.sqrt(self._npart)))
        for k in range(self._npart):
            ix = k/int(sn)
            iy = k - ix*int(sn)
            self.rx[k] = self._domain.xmin_int+(ix+0.5)/sn*self._domain.Lx
            self.ry[k] = self._domain.ymin_int+(iy+0.5)/sn*self._domain.Ly
    
    def enforce_periodicity(self):
        '''Ensure that all particles are in the interior of domain.

        For all particles which have left the interior of the domain, add 
        :math:`(\pm L_x,0)` or :math:`(0,\pm L_y)`.
        '''
        for i in range(self._npart):
            if (self.rx[i] >= self._domain.xmax_int):
                self.rx[i] -= self._domain.Lx
            if (self.rx[i] < self._domain.xmin_int):
                self.rx[i] += self._domain.Lx
            if (self.ry[i] >= self._domain.ymax_int):
                self.ry[i] -= self._domain.Ly
            if (self.ry[i] < self._domain.ymin_int):
                self.ry[i] += self._domain.Ly

    def duplicate(self):
        '''Add ghost particles on boundary of domain to account for periodic BCs.

        For all particles which have x-coordinates which is less than
        :math:`r_c` away for the (interior) domain boundary in the x-direction,
        add a duplicate particle at position :math:`(r_x\pm L_x,r_y)`. Note that the
        new position is on the boundary of the domain.
        Then repeat the process analogeous process in the y-direction for all particles
        (including the ones just generated).
        '''
        npart=self._npart
        j = npart
        for i in range(npart):
            # Particles in range (x_min_int,x_min_int+r_c) 
            if (self.rx[i] < self._domain.xmin_int+self._potential.rc):
                self.rx[j] = self.rx[i]+self._domain.Lx
                self.ry[j] = self.ry[i]
                self.vx[j] = self.vx[i]
                self.vy[j] = self.vy[i]
                j += 1
            # Particles in range (x_max_int-r_c,x_max_int) 
            if (self.rx[i] > self._domain.xmax_int-self._potential.rc):
                self.rx[j] = self.rx[i]-self._domain.Lx
                self.ry[j] = self.ry[i]
                self.vx[j] = self.vx[i]
                self.vy[j] = self.vy[i]
                j += 1 
        npart = j
        for i in range(npart):
            # Particles in range (y_min_int,y_min_int+r_c) 
            if (self.ry[i] < self._domain.ymin_int+self._potential.rc):
                self.rx[j] = self.rx[i]
                self.ry[j] = self.ry[i]+self._domain.Ly
                self.vx[j] = self.vx[i]
                self.vy[j] = self.vy[i]
                j += 1 
            # Particles in range (y_max_int-r_c,y_max_int) 
            if (self.ry[i] > self._domain.ymax_int-self._potential.rc):
                self.rx[j] = self.rx[i]
                self.ry[j] = self.ry[i]-self._domain.Ly
                self.vx[j] = self.vx[i]
                self.vy[j] = self.vy[i]
                j += 1
        self._npart_total = j

    def _build_layerlist(self):
        '''Populate the data structures for identifying particles sorted by layers.

        The indices of the particles in the different layers are stored in the
        following data structures:
        
        :math:`L[q_\ell,\dots,q_{\ell+1}]` contains the indices of all particles
        in layer :math:`\ell`. By construction, each layer only contains one (or zero)
        particles for each cell (including the boundary cells) of the computational domain.
        Furthermore for any position :math:`i` in the array :math:`L`, the 
        number :math:`b_i` is the index of the cell which contains the particle whose index
        is stored in :math:`L_i`.

        The arrays are labelled as follows:
            * :math:`L` = self._layerlist
            * :math:`q` = self._qoffset
            * :math:`b` = self._bmask
        '''
        i = 0
        for rx,ry in zip(self.rx[0:self._npart_total],
                         self.ry[0:self._npart_total]):
            self._cell_idx[i] = self._domain.cellidx(rx,ry)
            i += 1
        particles = set(range(self._npart_total))
        n = 0
        i = 0
        self._qoffset[0] = 0
        while (len(particles) > 0):
            p_layer = np.zeros(self._domain.ncells)-1
            for part in particles:
                p_layer[self._cell_idx[part]] = part
            for x in p_layer:
                if (x != -1):
                    particles.remove(x)
                    self._layerlist[n] = x
                    self._bmask[n] = self._cell_idx[x]
                    n += 1
            i+=1
            self._qoffset[i] = n

    def update_accelerations(self):
        '''Calculate accelerations on each particles.

        Loop over pairs of colours in the outer loop and over cells in the inner
        loop to update the forces acting on each particle.
        '''
        self.duplicate()
        self._build_layerlist()
        self.ax[:] = 0.0
        self.ay[:] = 0.0
        rc2 = self._potential.rc**2
        self._pe[0:self._npart] = 0.0
        for ell_1 in range(self._maxlayer):
            idx_1 = np.zeros(self._domain.ncells,dtype=int)-1
            for i in range(self._qoffset[ell_1],self._qoffset[ell_1+1]):
                idx_1[self._bmask[i]] = self._layerlist[i]
            for ell_2 in range(ell_1,self._maxlayer):
                idx_2 = np.zeros(self._domain.ncells,dtype=int)-1
                for i in range(self._qoffset[ell_2],self._qoffset[ell_2+1]):
                    idx_2[self._bmask[i]] = self._layerlist[i]
                if (ell_1 == ell_2):
                    k_min = 0
                    k_max = 3
                else:
                    k_min = 0
                    k_max = 8
                for k in range(k_min,k_max+1):
                    for m_1 in range(self._domain.ncells):
                        m_2 = m_1 + self._domain.offset[k]
                        if (self._domain.isinterior(m_1) or self._domain.isinterior(m_2)):
                            i_1 = idx_1[m_1]
                            i_2 = idx_2[m_2]
                            if ( (i_1 > -1) and (i_2 > -1) and (i_1 != i_2)):
                                dx = self.rx[i_1] - self.rx[i_2]
                                dy = self.ry[i_1] - self.ry[i_2]
                                dd = dx**2+dy**2
                                if (dd < rc2):
                                    pot = self._potential.evaluate(math.sqrt(dd))
                                    a_x,a_y = self._potential.evaluate_force(dx,dy)
                                    a_x *= self._inv_mass
                                    a_y *= self._inv_mass
                                    self.ax[i_1] += a_x
                                    self.ay[i_1] += a_y
                                    self.ax[i_2] -= a_x
                                    self.ay[i_2] -= a_y
                                    self._pe[i_1] += 0.5*pot
                                    self._pe[i_2] += 0.5*pot

    def savefig(self,filename,interior_only=False):
        '''Plot and save to file.

        Plot the domain and particles and save to file.

        :arg filename: Name of file to save to
        :arg interior_only: If this is true, only plot the interior particles.
            Otherwise show all particles
        '''
        self.duplicate()
        self._build_layerlist()
        plt.clf()
        self._domain.draw()
        for ell in range(self._maxlayer):
            imin = self._qoffset[ell]
            imax = self._qoffset[ell+1]
            rx = []
            ry = []
            for i in range(imin,imax):
                if (not interior_only) or (self._domain.isinterior(self._bmask[i])):
                    rx = np.append(rx,self.rx[self._layerlist[i]])
                    ry = np.append(ry,self.ry[self._layerlist[i]])
            plt.plot(rx,ry,marker='o',markersize=4,linewidth=0)
        plt.savefig(filename,bbox_inches='tight')
