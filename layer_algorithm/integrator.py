import state

class Integrator(object):
    '''Abstract base class for integrators.

    Class for time stepping algorithm

    :arg state: The Molecular dynamics state, has to be of type :class:`.State`
    :arg dt: Time step size
    '''
    def __init__(self,state,dt):
        self._state = state
        selt._dt = dt

    @property
    def state(self):
        '''Return current state.'''
        return self._state

    def step(self):
        pass

class Leapfrog(Integrator):
    '''Leapfrog integrator.

    :arg state: The molecular dynamics state, has to be of type :class:`.State`
    :arg dt: Time step size
    '''
    def __init__(self,state,dt):
        self._state = state
        self._dt = dt

    def step(self):
        npart = self._state.npart
        self._state.update_accelerations()
        self._state.vx[0:npart] += self._state.ax[0:npart]*self._dt
        self._state.vy[0:npart] += self._state.ay[0:npart]*self._dt
        self._state.rx[0:npart] += self._state.vx[0:npart]*self._dt
        self._state.ry[0:npart] += self._state.vy[0:npart]*self._dt
        self._state.enforce_periodicity()

