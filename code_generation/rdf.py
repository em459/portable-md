import math
from kernel import *
from constant import *
from all_pair_loop import *
from domain import *
from dat import *

class RDF(object):
    '''Radial distribution function.

    This class can be used to calculate a discrete form of 
    the radial distribution function, namely

        .. math: 
            g(r_n) = \\frac{V}{2\pi N_p^2 r_n^2 \Delta r_n} h_n

    where :math:`h_n` is the number of atom pairs (i,j) for which the
    interatomic distance :math:`r = |\\vec{r}_i-\\vec{r}_j|` falls into
    the range :math:`[(n-1)\Delta r,n \Delta r]` where
    :math:`n=0,\dots,n_{bin}-1`. :math:`V` is the simulation volume and 
    :math:`N_p` is the total number of particles
    in the simulation, the scaling ensures that :math:`g(r)\\rightarrow 1`
    as :math:`r\\rightarrow \infty`. Only distances up to :math:`r=r_{\max}` 
    are taken into account, where :math:`r_{\max}` is usually limited by the 
    size of the box that is studied.

    To use the class, the ``reset()`` method resets the calculation for 
    new measurements. The method ``accumulate()`` increases the binned
    counter and can be used multiple time. The ``finalise()`` method scales
    the binned distribution with the correct scaling factors.
    
    :arg domain: The computational domain
    :arg positions: Particle positions
    :arg rmax: Maximal radius
    :arg nbins: Number of bins used for sampling    
    '''
    def __init__(self,domain,positions,rmax,nbins=16):
        self._domain = domain
        self._positions = positions
        self._rmax = float(rmax)
        self._nbins = nbins
        self._dr = self._rmax/self._nbins
        self._r = (np.arange(self._nbins)+0.5)*self._dr
        self._bins = GlobalDat(self._nbins)
        kernel_code = '''
          int i;
          double dd = 0.0;
          for (i=0;i<3;++i) {
            double dx = p[0][i] - p[1][i];
            if (dx >  0.5*extent[i]) dx -= extent[i];
            if (dx < -0.5*extent[i]) dx += extent[i];
            dd += dx*dx;
          }
          if (dd < rmax2) bins[(int) floor(sqrt(dd)/dr)] += 1.0;
        '''
        extent = GlobalDat(3)
        extent.data[0] = self._domain.Lx
        extent.data[1] = self._domain.Ly
        extent.data[2] = self._domain.Lz
        kernel = Kernel('accumulate_rdf',kernel_code,
                        [Constant('rmax2',self._rmax**2),
                         Constant('dr',self._dr)])
        self._accumulation_loop = AllPairLoop(kernel,
                                              {'p':self._positions,
                                               'extent':extent,
                                               'bins':self._bins})
        self._cscale = self._domain.volume \
                     / (2.*math.pi*self._positions.npart**2*self._dr)
        self.reset()

    def reset(self):
        '''Reset the entries to zero to prepare for new measurement'''
        self.evaluations = 0
        self._bins.data[:] = 0.0

    def accumulate(self):
        '''Accumulate the rdf with the current particle positions'''
        assert(self.evaluations >= 0)
        self._accumulation_loop.execute()
        self.evaluations += 1

    def finalise(self):
        '''Scale rdf data with appropriate scaling factors.'''
        self._bins.data[:] *= self._cscale
        self._bins.data[:] /= self._r**2
        if (self.evaluations > 0):
            self._bins.data[:] /= self.evaluations
            self.evaluations = -1

    @property
    def data(self):
        '''Return binned data as numpy array'''
        return self._bins.data

    @property
    def r(self):
        '''Return bin coorindates :math:`r_i=(i+0.5)\Delta r` as numpy array'''
        return self._r
