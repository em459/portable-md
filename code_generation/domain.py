import numpy as np
from matplotlib import pyplot as plt
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d import Axes3D

class Domain(object):
    '''Computational domain of size :math:`V=L_x\\times L_y\\times L_z`
        subdivided into :math:`n=n_x\\times n_y\\times n_z` cells.

    The number of cells has to be at least 4 in each direction.
    Each cell index :math:`(i_x,i_y,iz)` is mapped to a linear index 
    :math:`i_x + n_x i_y + n_x n_y i_z`.

    :arg Lx: Size :math:`L_x`
    :arg Ly: Size :math:`L_y`
    :arg Lz: Size :math:`L_z`
    :arg nx: Number of cells in x-direction
    :arg ny: Number of cells in y-direction
    :arg nz: Number of cells in z-direction
    '''
    def __init__(self,Lx=1,Ly=1,Lz=1,nx=4,ny=4,nz=4):
        assert(nx>=4)
        assert(ny>=4)
        assert(nz>=4)
        self._Lx = float(Lx)
        self._Ly = float(Ly)
        self._Lz = float(Ly)
        self._nx = nx
        self._ny = ny
        self._nz = nz
        self._dict = {'NX':self._nx,
                      'NY':self._ny,
                      'NZ':self._nz,
                      'LX':self._Lx,
                      'LY':self._Ly,
                      'LZ':self._Lz,
                      'DX':self.dx,
                      'DY':self.dy,
                      'DZ':self.dz}

    @property
    def Lx(self):
        '''Size in x-direction'''
        return self._Lx

    @property
    def Ly(self):
        '''Size in y-direction'''
        return self._Ly

    @property
    def Lz(self):
        '''Size in z-direction'''
        return self._Lz

    @property
    def nx(self):
        '''Number of cells in x-direction'''
        return self._nx

    @property
    def ny(self):
        '''Number of cells in y-direction'''
        return self._ny

    @property
    def nz(self):
        '''Number of cells in z-direction'''
        return self._nz

    @property
    def dx(self):
        '''Cell size in x-direction.'''
        return self._Lx/self._nx

    @property
    def dy(self):
        '''Cell size in y-direction.'''
        return self._Ly/self._ny

    @property
    def dz(self):
        '''Cell size in z-direction.'''
        return self._Lz/self._nz

    @property
    def volume(self):
        '''Domain volume :math:`V=L_xL_yL_z`'''
        return self._Lx*self._Ly*self._Lz

    @property
    def ncells(self):
        '''Total number of cells :math:`n_x\\times n_y`'''
        return self._nx*self._ny*self._nz

    def code_cellidx(self,x,y,z):
        '''Return cell index of particle with position r=(x,y,z)'''
        s = '(int) (floor(%(X)s/%(DX)f) + %(NX)d*floor(%(Y)s/%(DY)f) + %(NX)d*%(NY)d*floor(%(Z)s/%(DZ)f))'
        d = {'X':str(x),'Y':str(y),'Z':str(z)}
        d.update(self._dict)
        return s % d

    def code_neighbour(self,i,j,direction,shift):
        '''Calculate neighbour cell index j and shift for a given cell i in 
            specified direction.

            This calculates the linear cell index of a cell in a given
            direction and writes the result to the integer variable j.
            It also returns the shift which has to be added to positions 
            to account for periodic BCs.

            Let the direction in the x-direction be dir_x = -1,0,+1
            and the direction in the y-direction dir_y=-1,0,+1.
            and the direction in the z-direction dir_z=-1,0,+1.
            Then direction = (dir_x+1) + 3*(dir_y+1) + 9*(dir_z+1).

        :arg i: Linear index of cell, can be a constant or variable name
        :arg j: Variable name of integer which will hold the linear index of
            the neighbouring cell
        :arg direction: direction in range 0,...,26.
        :arg shift: Variable name of real array (length 3) which will hold 
            the to be added to the particle position
        '''
        s = '''
          int iz = %(I)s;
          int dir = %(DIRECTION)s;
          int ix = iz %% %(NX)d + (dir %% 3)-1; 
          iz /= %(NX)d;
          dir /= 3;
          int iy = iz %% %(NY)d + (dir %% 3)-1;
          iz /= %(NY)d;
          iz += dir/3-1;
          int dx = 0;
          int dy = 0;
          int dz = 0;
          shift[0] = 0;
          shift[1] = 0;
          shift[2] = 0;
          if (ix < 0)        { dx = +%(NX)d; %(SHIFT)s[0] = -%(LX)f; }
          if (ix > %(NX)d-1) { dx = -%(NX)d; %(SHIFT)s[0] = +%(LX)f; }
          if (iy < 0)        { dy = +%(NY)d; %(SHIFT)s[1] = -%(LY)f; }
          if (iy > %(NY)d-1) { dy = -%(NY)d; %(SHIFT)s[1] = +%(LY)f; }
          if (iz < 0)        { dz = +%(NZ)d; %(SHIFT)s[2] = -%(LZ)f; }
          if (iz > %(NZ)d-1) { dz = -%(NZ)d; %(SHIFT)s[2] = +%(LZ)f; }
          %(J)s = (ix+dx) + %(NX)d*(iy+dy) +%(NX)d*%(NY)d*(iz+dz);
        '''
        d = {'I':str(i),
             'J':j,
             'DIRECTION':str(direction),
             'SHIFT':shift}
        d.update(self._dict)
        return s % d

    def draw(self):
        '''Draw the domain.

        Draw the entire domain, highlight the interior and show the individual cells.
        '''
        ax = plt.gca(projection='3d')
        x_list = (np.array([0,0]),
                  np.array([1,1]),
                  np.array([0,0]),
                  np.array([1,1]))
        y_list = (np.array([0,0]),
                  np.array([0,0]),
                  np.array([1,1]),
                  np.array([1,1]))
        z_list = (np.array([0,1]),
                  np.array([0,1]),
                  np.array([0,1]),
                  np.array([0,1]))
        for i in range(3):
            for x,y,z in zip(x_list,y_list,z_list):
                ax.plot(self._Lx*x, self._Ly*y, self._Lz*z,
                        color='black',linewidth=2)
            tmp_list = x_list
            x_list = y_list
            y_list = z_list
            z_list = tmp_list
        xticks = [i*self.Lx/float(self.nx) for i in range(self.nx+1)]
        yticks = [i*self.Ly/float(self.ny) for i in range(self.ny+1)]
        zticks = [i*self.Lz/float(self.nz) for i in range(self.nz+1)]
        ax.set_xticks(xticks)
        ax.set_yticks(yticks)
        ax.set_zticks(zticks)
        ax.set_xticklabels([('%4.2f' % i) for i in xticks])
        ax.set_yticklabels([('%4.2f' % i) for i in yticks])
        ax.set_zticklabels([('%4.2f' % i) for i in zticks])
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
