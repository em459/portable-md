import numpy as np
import math
from matplotlib import pyplot as plt

from dat import *
from domain import *
from potential import *
from pair_loop import *
from direct_loop import *

class State(object):
    '''State of atomic system.

    Stores the positions, velocities and accelerations of a set of particles
    confined to a two dimensional domain, which interact via a given short range
    potential. The class provides a method for calculating the interatomic forces.
    

    :arg domain: Computational domain, of type :class:`.Domain`
    :arg potential: Interatomic potential, has to be a type derived from the
        abstract base class :class:`.Potential`
    :arg npart: Number of particles
    :arg mass: Mass of one particle
    :arg temperature: Initial temperature
    '''
    def __init__(self,domain,potential,npart,temperature):
        self._domain = domain
        self._potential = potential
        # Number of interior particles
        self._npart = npart
        # Particle positions
        self.positions = ParticleDat(self._npart,3)
        self.velocities = ParticleDat(self._npart,3)
        self.masses = ParticleDat(self._npart,1)
        self.masses.assign(1.0)
        self.accelerations = ParticleDat(self._npart,3)
        self._potential_energy = GlobalDat(1)
        self._kinetic_energy = GlobalDat(1)
        self.arrange_positions()
        self._accelerations_pairloop = PairLoop(self._domain,
                                                self._potential.kernel_accelerations(),
                                                self.positions,{'p':self.positions,
                                                                'a':self.accelerations,
                                                                'm':self.masses})
        self._potential_energy_pairloop = PairLoop(self._domain,
                                                   self._potential.kernel_potential_energy(),
                                                   self.positions,{'p':self.positions,
                                                                   'Epot':self._potential_energy})
        kernel_code='Ekin[0] += 0.5*(v[0]*v[0]+v[1]*v[1]+v[2]*v[2])/m[0];'
        kinetic_energy_kernel = Kernel('calc_kinetic_energy',kernel_code)
        self._kinetic_energy_directloop = DirectLoop(kinetic_energy_kernel,
                                                     {'v':self.velocities,
                                                      'Ekin':self._kinetic_energy,
                                                      'm':self.masses})
        kernel_code = '''
          int i;
          while (p[0]>Lx) p[0] -= Lx; while (p[0]<0) p[0] += Lx;
          while (p[1]>Ly) p[1] -= Ly; while (p[1]<0) p[1] += Ly;
          while (p[2]>Lz) p[2] -= Lz; while (p[2]<0) p[2] += Lz;
        '''
        periodicity_kernel = Kernel('enforce_periodicty',kernel_code,
                                    [Constant('Lx',self._domain.Lx),
                                     Constant('Ly',self._domain.Ly),
                                     Constant('Lz',self._domain.Lz)])
        self._periodicity_directloop = DirectLoop(periodicity_kernel,
                                                  {'p':self.positions})
        self.randomize_velocities(temperature)

    @property
    def domain(self):
        '''Return computational domain.'''
        return self._domain

    @property
    def potential(self):
        '''Return interatomic potential.'''
        return self._potential

    @property
    def npart(self):
        '''Return number of particles.'''
        return self._npart

    def randomize_velocities(self,temperature):
        '''Set the velocities of the interior particles to random values.'''
        np.random.seed(239351397)
        kB = 1.0
        self.velocities.data[0:self.npart,:] = np.random.randn(self.npart,3)
        Ekin = self.kinetic_energy()
        self.velocities.data[:,:] *= math.sqrt(3.*kB*temperature/Ekin)
    
    def arrange_positions(self):
        '''Arrange the (interior) particle positions in a regular grid in the domain.'''
        sn = int(math.ceil(math.pow(self._npart,1./3.)))
        k = 0
        for ix in range(sn):
            for iy in range(sn):
                for iz in range(sn):
                    self.positions.data[k,0] = (ix+0.5)/sn*self._domain.Lx
                    self.positions.data[k,1] = (iy+0.5)/sn*self._domain.Ly
                    self.positions.data[k,2] = (iz+0.5)/sn*self._domain.Lz
                    k += 1
   
    def update_accelerations(self):
        self._accelerations_pairloop.build_cell_list()
        self.accelerations.data[:,:] = 0.0
        self._accelerations_pairloop.execute()

    def potential_energy(self):
        self._potential_energy_pairloop.build_cell_list()
        self._potential_energy.data[0] = 0.0
        self._potential_energy_pairloop.execute()
        return self._potential_energy.data[0]/self._npart

    def kinetic_energy(self):
        self._kinetic_energy.data[0] = 0.0
        self._kinetic_energy_directloop.execute()
        return self._kinetic_energy.data[0]/self._npart
 
    def enforce_periodicity(self):
        '''Ensure that all particles are in the interior of domain.

        For all particles which have left the interior of the domain, add 
        :math:`(\pm L_x,0)` or :math:`(0,\pm L_y)`.
        '''
        self._periodicity_directloop.execute()

    def savefig(self,filename):
        '''Plot and save to file.

        Plot the domain and particles and save to file.

        :arg filename: Name of file to save to
        '''
        plt.clf()
        ax = plt.gca(projection='3d')
        self._domain.draw()
        rx = self.positions.data[:,0]
        ry = self.positions.data[:,1]
        rz = self.positions.data[:,2]
        ax.scatter(rx,ry,rz,marker='o',color='blue')
        plt.savefig(filename,bbox_inches='tight')
