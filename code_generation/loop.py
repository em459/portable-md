import hashlib
import ctypes
import os
import numpy as np
import subprocess
from dat import *

class Loop(object):
    '''Base class for JIT compiled loops.

    This class provides basic functionality for just-in-time compilation
    of loops over particle sets.
 
    :arg kernel: kernel to wrap, has to be of type :class:`Kernel`
    :arg dats: Particle data, of type derived from :class:`Dat`
    :arg headers: Header files to include
    '''
    def __init__(self,kernel,dat_dict,headers=None):
        self._temp_dir = './build/'
        if (not os.path.exists(self._temp_dir)):
            os.mkdir(self._temp_dir)
        self._kernel = kernel
        self._dat_dict = dat_dict
        self._npart = None
        for key, dat in self._dat_dict.iteritems():
            if (type(dat) is ParticleDat):
                if (self._npart == None):
                    self._npart = dat.npart
                else:
                    assert(self._npart == dat.npart)
        assert(self._npart != None)
        self._nargs = len(self._dat_dict)
        self._headers = headers
        self._library_filename = self._unique_name()+'.so'

    def _initialise_library(self):
        '''Check if library already exists and create otherwise'''
        if (not os.path.exists(os.path.join(self._temp_dir,
                                            self._library_filename))):
            self._create_library()
        self._lib = np.ctypeslib.load_library(self._library_filename,
                                              self._temp_dir)
    def hexdigest(self):
        '''Create unique hex digest'''
        m = hashlib.md5()
        m.update(self._kernel.code)
        if (self._headers != None):
            for header in self._headers:
                m.update(header)
        return m.hexdigest()

    def _unique_name(self):
        '''Return name which can be used to identify the loop 
        in a unique way.
        '''
        return self._kernel.name+'_'+self.hexdigest()

    def _create_library(self):
        '''Create a shared library from the source code.
        '''
        filename_base = os.path.join(self._temp_dir,self._unique_name())
        header_filename = filename_base+'.h'
        impl_filename = filename_base+'.c'
        with open(header_filename,'w') as f:
            print >> f, self._generate_header_source()
        with open(impl_filename,'w') as f:
            print >> f, self._generate_impl_source()
        object_filename = filename_base+'.o'
        library_filename = filename_base+'.so'
        cflags = ['-O3','-fpic']
        cc = 'gcc'
        ld = 'gcc'
        compile_cmd = [cc,'-c','-fpic']+cflags+['-I',self._temp_dir] \
                       +['-o',object_filename,impl_filename]
        link_cmd = [ld,'-shared']+['-o',library_filename,object_filename]
        stdout_filename = filename_base+'.log'
        stderr_filename = filename_base+'.err'
        with open(stdout_filename,'w') as stdout:
            with open(stderr_filename,'w') as stderr:
                stdout.write('Compilation command:\n')
                stdout.write(' '.join(compile_cmd))
                stdout.write('\n\n')
                p = subprocess.Popen(compile_cmd,
                                     stdout=stdout,
                                     stderr=stderr)
                p.communicate()
                stdout.write('Link command:\n')
                stdout.write(' '.join(link_cmd))
                stdout.write('\n\n')
                p = subprocess.Popen(link_cmd,
                                     stdout=stdout,
                                     stderr=stderr)
                p.communicate()

    def _included_headers(self):
        '''Return names of included header files.'''
        s = ''
        if (self._headers != None):
            s += '\n'
            for x in self._headers:
                s += '#include \"'+x+'\"'
        return s

    def _argnames(self):
        '''Comma separated string of argument name declarations.

        This string of argument names is used in the declaration of 
        the method which executes the loop over the grid. 
        If, for example, the loop gets passed two dats, 
        then the result will be ``double* arg_000,double* arg_001`.`
        '''
        argnames = ''
        for i in range(self._nargs):
            argnames += 'double *arg_'+('%03d' % i)+','
        return argnames[:-1]

    def _loc_argnames(self):
        '''Comma separated string of local argument names.

        This string is used in the call to the local kernel. If, for
        example, two particle dats get passed to the pairloop, then
        the result will be ``loc_arg_000,loc_arg_001``. Each of these
        is of type ``double* [2]``, see method _kernel_argument_declarations()
        '''
        argnames = ''
        for i in range(self._nargs):
            argnames += 'loc_arg_'+('%03d' % i)+','
        return argnames[:-1]
