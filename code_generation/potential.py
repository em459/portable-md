import numpy as np
from kernel import *
from constant import *
from pair_loop import *

class LennartJones(object):
    def __init__(self,domain,sigma,epsilon):
        self._domain = domain
        self._sigma = sigma
        self._epsilon = epsilon
        self._sigma2 = self._sigma**2
        self._rc = 2.**(1./6.)*self._sigma

    def kernel_accelerations(self):
        kernel_code = '''
          double dx[3];
          int i;
          double dd = 0.0;
          for (i=0;i<3;++i) {
            dx[i] = p[0][i] - p[1][i] - shift[i];
            dd += dx[i]*dx[i];
          }
          if (dd < rc2) {
            double r_m2 = sigma2/dd;
            double r_m4 = r_m2*r_m2;
            double r_m6 = r_m2*r_m4;
            double r_m8 = r_m4*r_m4;
            double tmp = C_F*(r_m6-0.5)*r_m8;
            for (i=0;i<3;++i) {
              a[0][i] += tmp/m[0][0]*dx[i];
              a[1][i] -= tmp/m[1][0]*dx[i];
            }
          }
        '''
        constants=(Constant('sigma2',self._sigma**2),
                   Constant('rc2',self._rc**2),
                   Constant('C_F',48.*self._epsilon/self._sigma**2))
        return Kernel('calc_accelerations',kernel_code,constants)

    def kernel_potential_energy(self):
        kernel_code = '''
          double dx;
          int i;
          double dd = 0.0;
          for (i=0;i<3;++i) {
            dx = p[0][i] - p[1][i] - shift[i];
            dd += dx*dx;
          }
          if (dd < rc2) {
            double r_m2 = sigma2/dd;
            double r_m4 = r_m2*r_m2;
            double r_m6 = r_m2*r_m4;
            double tmp = C_V*((r_m6-1.0)*r_m6+0.25);
            Epot[0] += 2.*tmp;
          }
        '''
        constants=(Constant('sigma2',self._sigma**2),
                   Constant('rc2',self._rc**2),
                   Constant('C_V',2.*self._epsilon))
        return Kernel('calc_potential_energy',kernel_code,constants)
