from dat import *
from constant import *
from kernel import *
from pair_loop import *
from potential import *
from domain import *
from state import *
from integrator import *
import numpy as np
import sys
import math

np.random.seed(1234124127)

def outputfilename(outputdir,i):
    return os.path.join(outputdir,'state_'+('%03d' % i)+'.png')

# Properties of Lennart Jones potential (dimensionless units)
epsilon = 1.0
sigma = 1.0

# density
rho = 0.9
# temperature
temperature = 1.0
# Number of unit cells in x-, y-, and z-direction
nu_x = 16
nu_y = 16
nu_z = 16
# Number of particles
npart = nu_x*nu_y*nu_z
Lx = nu_x*rho**(-1./3.)
Ly = nu_y*rho**(-1./3.)
Lz = nu_z*rho**(-1./3.)
rc = 2.**(1./6.)*sigma
nx = int(math.ceil(Lx/rc))
ny = int(math.ceil(Ly/rc))
nz = int(math.ceil(Lz/rc))
dt=0.0005
nsteps = 64

domain = Domain(Lx,Ly,Ly,nx,ny,nz)
domain.draw()

potential = LennartJones(domain,sigma,epsilon)

state = State(domain,potential,npart,temperature)
state.savefig('state.pdf')

integrator = VelocityVerlet(state,dt)

outputdir=os.path.join(os.path.expanduser('~'),'tmp/output_md')
for i in range(nsteps):
    ke = state.kinetic_energy()
    pe = state.potential_energy()
    print i, pe, ke, ke+pe
    state.savefig(outputfilename(outputdir,i))
    integrator.step()
