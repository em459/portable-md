import state

class Integrator(object):
    '''Abstract base class for integrators.

    Class for time stepping algorithm

    :arg state: The Molecular dynamics state, has to be of type :class:`.State`
    :arg dt: Time step size
    '''
    def __init__(self,state,dt):
        self._state = state
        selt._dt = dt

    @property
    def state(self):
        '''Return current state.'''
        return self._state

    def step(self):
        pass

class Leapfrog(Integrator):
    '''Leapfrog integrator.

    :arg state: The molecular dynamics state, has to be of type :class:`.State`
    :arg dt: Time step size
    '''
    def __init__(self,state,dt):
        self._state = state
        self._dt = dt

    def step(self):
        npart = self._state.npart
        self._state.update_accelerations()
        self._state.velocities.data[:,:] += self._state.accelerations.data[:,:]*self._dt
        self._state.positions.data[:,:] += self._state.velocities.data[:,:]*self._dt
        self._state.enforce_periodicity()

class VelocityVerlet(Integrator):
    '''Velocity Verlet integrator.

    :arg state: The molecular dynamics state, has to be of type :class:`.State`
    :arg dt: Time step size
    '''
    def __init__(self,state,dt):
        self._state = state
        self._dt = dt
        self._dt_half = 0.5*self._dt

    def step(self):
        npart = self._state.npart
        self._state.velocities.data[:,:] += self._state.accelerations.data[:,:]*self._dt_half
        self._state.positions.data[:,:] += self._state.velocities.data[:,:]*self._dt
        self._state.enforce_periodicity()
        self._state.update_accelerations()
        self._state.velocities.data[:,:] += self._state.accelerations.data[:,:]*self._dt_half
