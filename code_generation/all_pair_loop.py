import ctypes
import numpy as np
from domain import *
from loop import *

class AllPairLoop(Loop):
    '''Execute two-particle kernel over all pairs of particles

    This is used to create a wrapper around a two-particle kernel,
    which is provided as a C-code, compile this into a shared library,
    and execute it over all particles for a given set of dats.
    In contrast to :class:`PairLoop` all pairs (and not only those in
    neighbouring cells) are considered. Hence this algorithm is O(n^2).

    The kernel has to have the signature
    ``kernel(double **loc_arg0,...,double **loc_argn)`` where 
    ``loc_arg[i][j]`` is a pointer to the entry j in the vector-value
    data of particle i (with i=0,1 since it is a pairwise kernel).
 
    :arg kernel: kernel to wrap, has to be of type :class:`Kernel`
    :arg dats: Particle data, of type derived from :class:`Dat`
    :arg headers: Header files to include
    '''
    def __init__(self,kernel,dat_dict,headers=None):
        super(AllPairLoop,self).__init__(kernel,dat_dict,headers)
        self._initialise_library()

    def execute(self):
        '''Execute the kernel over all particle pairs.'''
        args = []

        for dat in self._dat_dict.values():
            args.append(dat._data.ctypes.data_as(ctypes.POINTER(ctypes.c_double)))
        method = self._lib[self._kernel.name+'_wrapper']
        method(*args)
    
    def _generate_impl_source(self):
        '''Generate the source code the actual implementation.'''
        code = '''
        #include \"%(UNIQUENAME)s.h\"

        %(KERNEL_METHODNAME)s {
        %(KERNEL)s
        }

        void %(KERNEL_NAME)s_wrapper(%(ARGUMENTS)s) { 
          int i,j;
          // Loop over all cells i = 0,...N_p-1
          for (i=0; i<%(NPART)d; ++i) {
            // Loop over all cells j = i+1,...N_p-1
            for (j=i+1; j<%(NPART)d; ++j) {
               %(KERNEL_ARGUMENT_DECL)s
               %(KERNEL_NAME)s(%(LOC_ARGUMENTS)s);
            }
          }
        }
        '''
        d = {'UNIQUENAME':self._unique_name(),
             'KERNEL_METHODNAME':self._kernel_methodname(),
             'KERNEL':self._kernel.code,
             'ARGUMENTS':self._argnames(),
             'LOC_ARGUMENTS':self._loc_argnames(),
             'KERNEL_NAME':self._kernel.name,
             'NPART':self._npart,
             'KERNEL_ARGUMENT_DECL':self._kernel_argument_declarations()}
        return code % d

    def _kernel_methodname(self):
        '''Construct the name of the kernel method.
        
        Return a string of the form 
        ``inline void kernel_name(double **<arg1>, double **<arg2}, ...) {``
        which is used for defining the name of the kernel method.
        '''
        space = ' '*14
        s = 'inline void '+self._kernel.name+'('
        for var_name, dat in self._dat_dict.iteritems():
            if (type(dat) is ParticleDat):
                s += 'double **'+var_name+', '
            else:
                s += 'double *'+var_name+', '
        s = s[:-2] + ')'
        return s

    def _generate_header_source(self):
        '''Generate the source code of the header file.

        Returns the source code for the header file.
        '''
        code = '''
        #ifndef %(UNIQUENAME)s_H
        #define %(UNIQUENAME)s_H %(UNIQUENAME)s_H

        %(INCLUDED_HEADERS)s
        #include <stdio.h>

        void %(KERNEL_NAME)s_wrapper(%(ARGUMENTS)s);

        #endif
        '''
        d = {'UNIQUENAME':self._unique_name(),
             'INCLUDED_HEADERS':self._included_headers(),
             'KERNEL_NAME':self._kernel.name,
             'ARGUMENTS':self._argnames()}
        return (code % d)

    def _kernel_argument_declarations(self):
        '''Define and declare the kernel arguments.

        For each argument the kernel gets passed a pointer of type
        ``double* loc_argXXX[2]``. Here ``loc_arg[i]`` with i=0,1 is
        pointer to the data which contains the properties of particle i.
        These properties are stored consecutively in memory, so for a 
        scalar property only ``loc_argXXX[i][0]`` is used, but for a vector
        property the vector entry j of particle i is accessed as 
        ``loc_argXXX[i][j]``.

        This method generates the definitions of the ``loc_argXXX`` variables
        and populates the data to ensure that ``loc_argXXX[i]`` points to
        the correct address in the dats.
        '''
        s = '\n'
        for i,dat in enumerate(self._dat_dict.values()):
            ncomp = dat.ncomp
            space = ' '*14
            argname = 'arg_'+('%03d' % i)
            loc_argname = 'loc_'+argname
            if (type(dat) is ParticleDat):
                s += space+'double *'+loc_argname+'[2];\n'
                s += space+loc_argname+'[0] = '+argname+'+'+str(ncomp)+'*i;\n'
                s += space+loc_argname+'[1] = '+argname+'+'+str(ncomp)+'*j;\n'
            else:
                s += space+'double *'+loc_argname+';\n'
                s += space+loc_argname+' = '+argname+';\n'
        return s
