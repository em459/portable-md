import sys
import ctypes
import numpy as np

class Dat(object):
    '''Storage for variables, either global or local'''
    def __init__(self,ncomp):
        self._ncomp = ncomp

    @property
    def ncomp(self):
        '''Return number of components.'''
        return self._ncomp

    @property
    def data(self):
        '''Return data, i.e. numpy array that is wrapped by the type.'''
        return self._data

    def ctypes_data(self):
        '''Return ctypes-pointer to data.'''
        return self._data.ctypes.data_as(ctypes.POINTER(ctypes.c_double))

class GlobalDat(Dat):
    '''Storage for global variables.

    Stores the a global float-vector-valued property in a
    numpy-array. The data has ``ncomp`` vector values, which are held in
    a numpy array.

    Data is initialised to zero

    :arg ncomp: Number of components per particle
    '''
    def __init__(self,ncomp):
        super(GlobalDat,self).__init__(ncomp)
        self._data = np.zeros(self._ncomp,dtype=float)

    def assign(self,value):
        '''Assign a constant value to data array'''
        self._dat[:] = value

class ParticleDat(Dat):
    '''Storage for particle properties.

    Stores the float-vector-valued properties of a set of particles in a
    numpy-array. For ``npart`` particles, each of which has a property of
    ``ncomp`` vector values, it creates a numpy array of size
    npart x ncomp. Since C-ordering (row-major-ordering) is used, all
    properties of a particle are stored consecutively in memory.

    :arg ncomp: Number of components per particle
    :arg random: Initialise to random values?
    '''
    def __init__(self,npart,ncomp,random=False):
        super(ParticleDat,self).__init__(ncomp)
        self._npart = npart
        self._random = random
        if self._random:
            self._data = np.random.rand(self._npart,self._ncomp)
        else:
            self._data = np.zeros((self._npart,self._ncomp),dtype=float)

    @property
    def npart(self):
        '''Return number of particles.'''
        return self._npart

    def assign(self,value):
        '''Assign a constant value to data array'''
        self._data[:,:] = value
